<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio de Sesión Fidelización</title>
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
</head>

<body class="align-middle">

    <section>
        <div class="row">
            <div class="col-6">
                <p style="padding: 50px 25px; text-align:justify">
                    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.
                </p>
            </div>
            <div class="col-4">
                <form action="" class="form">
                    <div class="text-center mb-4">
                        <img src="img/logo.png" alt="Logo SENA" width="80" height="80">
                        <h1 class="h3 mb-3 font-weight-normal">Aplicación de Fidelización de Aprendices</h1>
                        <p>Servicio Nacional de Aprendizaje SENA</p>
                    </div>
                    <div class="form-label-group">
                        <label for="email">Correo Electronico</label>
                        <input type="email" id="email" name="email" class="form-control form-lg"
                            placeholder="Ingrese su Correo @">
                    </div>
                    <div class="form-label-group">
                        <label for="password">Contraseña</label>
                        <input type="password" id="password" name="password" class="form-control form-lg">
                    </div>
                    <div class="mb-3">
                        <button class="btn btn-lg btn-primary text-center">
                            Iniciar Sesión
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>

</html>